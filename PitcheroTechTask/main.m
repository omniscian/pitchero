//
//  main.m
//  PitcheroTechTask
//
//  Created by Ian Layland-Houghton on 24/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
