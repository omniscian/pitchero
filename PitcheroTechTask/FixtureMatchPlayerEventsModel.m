//
//  FixtureMatchPlayerEventsModel.m
//  PitcheroTechTask
//
//  Created by Ian Houghton on 25/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import "FixtureMatchPlayerEventsModel.h"

@implementation FixtureMatchPlayerEventsModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        if (dictionary[@"player_event_id"] != [NSNull null]) {
            self.playerEventId = [dictionary[@"player_event_id"] integerValue];
        }
        
        if (dictionary[@"event_type_id"] != [NSNull null]) {
            self.eventTypeId = [dictionary[@"event_type_id"] integerValue];
        }
        
        if ([dictionary[@"fixture_id"] length] > 0) {
            self.fixtureId = dictionary[@"fixture_id"];
        }
        
        if (dictionary[@"member_id"] != [NSNull null]) {
            self.memberId = [dictionary[@"member_id"] integerValue];
        }
        
        if (dictionary[@"team_id"] != [NSNull null]) {
            self.teamId = [dictionary[@"team_id"] integerValue];
        }
        
        if (dictionary[@"second"] != [NSNull null]) {
            self.second = [dictionary[@"second"] integerValue];
        }
        
        if (dictionary[@"period"] != [NSNull null]) {
            self.period = [dictionary[@"period"] integerValue];
        }
    }
    
    return self;
}

@end
