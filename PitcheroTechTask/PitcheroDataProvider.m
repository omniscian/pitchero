//
//  PitcheroDataProvider.m
//  PitcheroTechTask
//
//  Created by Ian Layland-Houghton on 24/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import "PitcheroDataProvider.h"
#import "FixtureMatchPlayerEventsModel.h"
#import "VideoModel.h"

NSString * const kBaseUrl = @"https://api.pitchero.com/v2/";
NSString * const kAuthorizationHeader = @"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyNDM4NDgyIiwiaXNzIjoiaHR0cDpcL1wvYXBpLnBpdGNoZXJvLmNvbVwvdjJcL2F1dGhcL2p3dCIsImlhdCI6MTQ5MDM1Mjg0MSwiZXhwIjoxNDkwOTU0MDQxLCJuYmYiOjE0OTAzNTI4NDEsImp0aSI6ImQ1MDA3Y2YxMGY5ZTU2OGFmYjE2NTdlMjY2ODZkMWJjIn0.gNfUB-vlaJnnt2NaZLicY-2QdYGZWq6GNcqzEg4-f4I";


@implementation PitcheroDataProvider

- (void)fetchMatchEventsForTeamId:(NSString *)teamId
                        fixtureId:(NSString *)fixtureId
                     successBlock:(void (^)(NSArray<FixtureMatchPlayerEventsModel *> *))successBlock
                     failureBlock:(void (^)())failureBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@match-player-events?fixture_id=%@&team_id=%@", kBaseUrl, fixtureId, teamId];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:kAuthorizationHeader forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data)
            {
                NSArray *eventArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSMutableArray *result = [NSMutableArray array];
                
                for (NSDictionary *eventDict in eventArray)
                {
                    FixtureMatchPlayerEventsModel *model = [[FixtureMatchPlayerEventsModel alloc] initWithDictionary:eventDict];
                    [result addObject:model];
                }
                
                successBlock(result);
            }
            else
            {
                failureBlock();
            }
        });
        
    }];
    
    [dataTask resume];
}

- (void)postVideoToPitchero:(VideoModel *)videoModel
               successBlock:(void (^)(NSString *))successBlock
               failureBlock:(void (^)())failureBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@videos", kBaseUrl];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:kAuthorizationHeader forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"POST"];

    NSData *postData = [NSJSONSerialization dataWithJSONObject:[videoModel jsonDictionaryForModel] options:0 error:nil];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data)
            {
                NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                if (!jsonDict[@"errors"])
                {
                    successBlock(jsonDict[@"video_id"]);
                }
                else
                {
                    failureBlock();
                }
            }
            else
            {
                failureBlock();
            }
        });
        
    }];
    
    [dataTask resume];
}

- (void)createMatchPlayerEventsForTeamId:(NSString *)teamId
                         fixtureId:(NSString *)fixtureId
                           seconds:(NSInteger)seconds
                      successBlock:(void (^)(NSString *playerEventId))successBlock
                      failureBlock:(void (^)())failureBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@match-player-events", kBaseUrl];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:kAuthorizationHeader forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *postBody = @{@"event_type_id": [NSNumber numberWithInt:2],
                               @"fixture_id": fixtureId,
                               @"member_id": [NSNumber numberWithInt:2957873],
                               @"team_id": [NSNumber numberWithInteger:[teamId integerValue]],
                               @"second": [NSNumber numberWithInteger:seconds],
                               @"period": [NSNumber numberWithInt:1]};
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:postBody options:0 error:nil];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data)
            {
                NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                if (!jsonDict[@"errors"])
                {
                    successBlock(jsonDict[@"player_event_id"]);
                }
                else
                {
                    failureBlock();
                }
            }
            else
            {
                failureBlock();
            }
        });
        
    }];
    
    [dataTask resume];
}

- (void)attachVideoToMatchEventsWithPlayerEventId:(NSString *)playerEventId
                                     videoEventId:(NSString *)videoEventId
                                     successBlock:(void (^)())successBlock
                                     failureBlock:(void (^)())failureBlock

{
    NSString *urlString = [NSString stringWithFormat:@"%@match-player-events/%@/video", kBaseUrl, playerEventId];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:kAuthorizationHeader forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *postBody = @{@"video_id":videoEventId};
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:postBody options:0 error:nil];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data)
            {
                NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                if ([jsonDict[@"code"] integerValue] != 404)
                {
                    successBlock();
                }
                else
                {
                    failureBlock();
                }
                
            }
            else
            {
                failureBlock();
            }
        });
        
    }];
    
    [dataTask resume];
}

@end
