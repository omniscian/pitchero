//
//  SourceModel.m
//  PitcheroTechTask
//
//  Created by Ian Houghton on 26/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import "SourceModel.h"
#import "AmazonS3Provider.h"

@implementation SourceModel

- (NSDictionary *)jsonDictionaryForModel
{
    NSDictionary *dictionary = @{@"type": @"S3",
                                 @"bucket": [AmazonS3Provider bucket],
                                 @"video_key": self.videoKey,
                                 @"thumbnail_key": self.thumbnailKey};
    
    return dictionary;
}

@end
