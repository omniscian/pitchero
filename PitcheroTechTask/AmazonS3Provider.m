//
//  AmazonS3Provider.m
//  PitcheroTechTask
//
//  Created by Ian Layland-Houghton on 24/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import "AmazonS3Provider.h"
#import <AWSS3/AWSS3.h>

@implementation AmazonS3Provider

+ (NSString *)bucket
{
    return @"mobile-tech-test-pitchero-com";
}

- (void)uploadFile:(NSURL *)filePath videoName:(NSString *)videoName completion:(completionBlock)completion
{
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = [AmazonS3Provider bucket];
    uploadRequest.key = videoName;
    uploadRequest.body = filePath;
    
    [[transferManager upload:uploadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor]
                                                       withBlock:^id(AWSTask *task) {
       if (task.error) {
           if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]) {
               switch (task.error.code) {
                   case AWSS3TransferManagerErrorCancelled:
                   case AWSS3TransferManagerErrorPaused:
                       break;
                       
                   default:
                       NSLog(@"Error: %@", task.error);
                       break;
               }
           } else {
               // Unknown error.
               NSLog(@"Error: %@", task.error);
           }
           
           completion(NO);
       }
       
       if (task.result) {
           completion(YES);
       }
       return nil;
    }];

}

@end
