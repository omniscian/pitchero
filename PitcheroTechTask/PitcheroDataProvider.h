//
//  PitcheroDataProvider.h
//  PitcheroTechTask
//
//  Created by Ian Layland-Houghton on 24/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FixtureMatchPlayerEventsModel, VideoModel;

@interface PitcheroDataProvider : NSObject

- (void)fetchMatchEventsForTeamId:(NSString *)teamId
                        fixtureId:(NSString *)fixtureId
                     successBlock:(void (^)(NSArray <FixtureMatchPlayerEventsModel *> *results))successBlock
                     failureBlock:(void (^)())failureBlock;

- (void)postVideoToPitchero:(VideoModel *)videoModel
               successBlock:(void (^)(NSString * videoId))successBlock
               failureBlock:(void (^)())failureBlock;

- (void)createMatchPlayerEventsForTeamId:(NSString *)teamId
                               fixtureId:(NSString *)fixtureId
                                 seconds:(NSInteger)seconds
                            successBlock:(void (^)(NSString *playerEventId))successBlock
                            failureBlock:(void (^)())failureBlock;

- (void)attachVideoToMatchEventsWithPlayerEventId:(NSString *)playerEventId
                                     videoEventId:(NSString *)videoEventId
                                     successBlock:(void (^)())successBlock
                                     failureBlock:(void (^)())failureBlock;

@end
