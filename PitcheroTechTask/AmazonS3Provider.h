//
//  AmazonS3Provider.h
//  PitcheroTechTask
//
//  Created by Ian Layland-Houghton on 24/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^completionBlock)(BOOL success);

@interface AmazonS3Provider : NSObject

+ (NSString *)bucket;
- (void)uploadFile:(NSURL *)filePath videoName:(NSString *)videoName completion:(completionBlock)completion;

@end
