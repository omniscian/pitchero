//
//  SourceModel.h
//  PitcheroTechTask
//
//  Created by Ian Houghton on 26/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SourceModel : NSObject

@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *bucket;
@property (nonatomic, strong) NSString *videoKey;
@property (nonatomic, strong) NSString *thumbnailKey;

- (NSDictionary *)jsonDictionaryForModel;

@end
