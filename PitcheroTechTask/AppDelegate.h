//
//  AppDelegate.h
//  PitcheroTechTask
//
//  Created by Ian Layland-Houghton on 24/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

