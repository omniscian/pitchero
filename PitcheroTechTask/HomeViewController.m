//
//  ViewController.m
//  PitcheroTechTask
//
//  Created by Ian Layland-Houghton on 24/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import "HomeViewController.h"

#import <MobileCoreServices/UTCoreTypes.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "FixtureMatchPlayerEventsModel.h"
#import "HomeViewModel.h"


@interface HomeViewController ()

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) HomeViewModel *viewModel;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    __weak typeof (self) weakSelf = self;
    
    self.viewModel = [[HomeViewModel alloc] init];
    [self.viewModel getLatestMatchEventsWithSuccess:^{
        [weakSelf.tableView reloadData];
    } failure:^{
        [weakSelf showAlertControllerWithTitle:@"Error" message:@"Unable to fetch match events"];
    }];
}

#pragma mark - UITableViewDatasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.viewModel.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"eventsCell"];
    
    FixtureMatchPlayerEventsModel *model = [self.viewModel.dataSource objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%ld", (long)model.playerEventId];
    
    return cell;
}

#pragma mark - IBActions

- (IBAction)recordButton:(id)sender {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        picker.allowsEditing = NO;
        
        NSArray *mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeMovie, nil];
        picker.mediaTypes = mediaTypes;
        
        [self presentViewController:picker animated:YES completion:nil];
    }
    else
    {
        [self showAlertControllerWithTitle:@"Error" message:@"Unable to record on this device."];
    }
}

- (void)streamVideoAtUrl:(NSURL *)videoUrl
{
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
    
    AVURLAsset *asset = [AVURLAsset assetWithURL: videoUrl];
    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
    
    AVPlayer * player = [[AVPlayer alloc] initWithPlayerItem:item];
    playerViewController.player = player;
    [playerViewController.view setFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.width)];
    
    playerViewController.showsPlaybackControls = YES;
    
    [self.view addSubview:playerViewController.view];
    
    [player play];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSURL *chosenMovie = [info objectForKey:UIImagePickerControllerMediaURL];
    
    __weak typeof (self) weakSelf = self;
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Enter details"
                                                                              message: @"Input title and description"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Title";
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Description";
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * titleTextfield = textfields[0];
        UITextField * descriptionTextfield = textfields[1];
        
        [weakSelf.viewModel uploadVideo:chosenMovie title:titleTextfield.text description:descriptionTextfield.text success:^{
            [weakSelf showAlertControllerWithTitle:@"Success" message:@"Video successfully uploaded."];
        } failure:^{
            [weakSelf showAlertControllerWithTitle:@"Error" message:@"Unable to upload. Please try again."];
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Helpers

- (void)showAlertControllerWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertView addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alertView animated:YES completion:nil];
}

@end
