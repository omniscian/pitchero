//
//  PostVideoModel.m
//  PitcheroTechTask
//
//  Created by Ian Houghton on 26/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import "VideoModel.h"

@implementation VideoModel

- (NSDictionary *)jsonDictionaryForModel
{
    NSDictionary *dictionary = @{@"title":self.videoTitle,
                                 @"description": self.videoTitle,
                                 @"height": [NSNumber numberWithInteger:self.height],
                                 @"width": [NSNumber numberWithInteger:self.width],
                                 @"duration": [NSNumber numberWithInteger:self.duration],
                                 @"origin": self.origin,
                                 @"source": [self.source jsonDictionaryForModel]};
    
    return dictionary;
}

@end
