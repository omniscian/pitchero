//
//  PostVideoModel.h
//  PitcheroTechTask
//
//  Created by Ian Houghton on 26/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SourceModel.h"

@interface VideoModel : NSObject

@property (nonatomic, strong) NSString *videoTitle;
@property (nonatomic, strong) NSString *videoDescription;
@property (nonatomic, assign) NSInteger height;
@property (nonatomic, assign) NSInteger width;
@property (nonatomic, assign) NSInteger duration;
@property (nonatomic, strong) NSString *origin;
@property (nonatomic, strong) SourceModel *source;
@property (nonatomic, strong) NSString *videoId;
@property (nonatomic, strong) NSString *playerEventId;

- (NSDictionary *)jsonDictionaryForModel;

@end
