//
//  HomeViewModel.h
//  PitcheroTechTask
//
//  Created by Ian Houghton on 26/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FixtureMatchPlayerEventsModel;

@interface HomeViewModel : NSObject

@property (nonatomic, strong) NSArray <FixtureMatchPlayerEventsModel *> *dataSource;

- (void)getLatestMatchEventsWithSuccess:(void (^)())success
                                failure:(void (^)())failure;

- (void)uploadVideo:(NSURL *)videoUrl
              title:(NSString *)title
        description:(NSString *)description
            success:(void (^)())successBlock
            failure:(void (^)())failureBlock;

@end
