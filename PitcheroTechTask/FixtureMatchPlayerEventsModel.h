//
//  FixtureMatchPlayerEventsModel.h
//  PitcheroTechTask
//
//  Created by Ian Houghton on 25/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FixtureMatchPlayerEventsModel : NSObject

@property (nonatomic, assign) NSInteger playerEventId;
@property (nonatomic, assign) NSInteger eventTypeId;
@property (nonatomic, strong) NSString *fixtureId;
@property (nonatomic, assign) NSInteger memberId;
@property (nonatomic, assign) NSInteger teamId;
@property (nonatomic, assign) NSInteger second;
@property (nonatomic, assign) NSInteger period;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithDictionary:(NSDictionary *)dictionary NS_DESIGNATED_INITIALIZER;

@end
