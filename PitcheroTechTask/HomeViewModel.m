//
//  HomeViewModel.m
//  PitcheroTechTask
//
//  Created by Ian Houghton on 26/03/2017.
//  Copyright © 2017 Ian Layland-Houghton. All rights reserved.
//

#import "HomeViewModel.h"
#import "PitcheroDataProvider.h"
#import "AmazonS3Provider.h"
#import "VideoModel.h"
#import <AVFoundation/AVFoundation.h>

NSString * const kFixtureId = @"0-3651243";
NSString * const kTeamId = @"172866";

@interface HomeViewModel ()
@property (nonatomic, strong) PitcheroDataProvider *pitcheroDataProvider;
@property (nonatomic, strong) AmazonS3Provider *amazonDataProvider;
@property (nonatomic, strong) VideoModel *videoToUploadModel;
@property (nonatomic, strong) NSURL *videoToUploadFilePath;
@property (nonatomic, strong) NSString *videoToUploadName;
@property (nonatomic, assign) NSInteger seconds;
@end

@implementation HomeViewModel

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        __weak typeof (self) weakSelf = self;
        
        [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
            weakSelf.seconds++;
        }];
    }
    
    return self;
}

- (PitcheroDataProvider *)pitcheroDataProvider
{
    if (!_pitcheroDataProvider) {
        _pitcheroDataProvider = [[PitcheroDataProvider alloc] init];
    }
    
    return _pitcheroDataProvider;
}

- (AmazonS3Provider *)amazonDataProvider
{
    if (!_amazonDataProvider) {
        _amazonDataProvider = [[AmazonS3Provider alloc] init];
    }
    
    return _amazonDataProvider;
}

- (void)getLatestMatchEventsWithSuccess:(void (^)())success
                                failure:(void (^)())failure
{
    __weak typeof (self) weakSelf = self;
    
    [self.pitcheroDataProvider fetchMatchEventsForTeamId:kTeamId fixtureId:kFixtureId successBlock:^(NSArray <FixtureMatchPlayerEventsModel *> *results) {
        weakSelf.dataSource = results;
        success();
    } failureBlock:^{
        failure();
    }];
}

- (void)uploadVideo:(NSURL *)videoUrl
              title:(NSString *)title
        description:(NSString *)description
            success:(void (^)())successBlock
            failure:(void (^)())failureBlock
{
    [self writeFileToDiskAndUploadVideoToAWS:videoUrl
                                     success:successBlock
                                     failure:failureBlock];
    
    [self setupVideoToUploadModelWithTitle:title
                               description:description
                             videoFilePath:self.videoToUploadFilePath
                                 videoName:self.videoToUploadName
                                   success:successBlock
                                   failure:failureBlock];
}

- (void)writeFileToDiskAndUploadVideoToAWS:(NSURL *)videoFilePath
                                   success:(void (^)())successBlock
                                   failure:(void (^)())failureBlock
{
    __weak typeof (self) weakSelf = self;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"PitcheroVideos"];
    
    NSError *error = nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory]){
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMddHHmmss"];
    
    NSString *videoName = [NSString stringWithFormat:@"%@.mov", [dateFormat stringFromDate:[NSDate date]]];
    NSURL *filePath = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", documentsDirectory, videoName]];
    
    self.videoToUploadName = videoName;
    self.videoToUploadFilePath = filePath;
    
    NSData *movieData = [NSData dataWithContentsOfURL:videoFilePath];
    [movieData writeToURL:filePath atomically:YES];
    
    [self.amazonDataProvider uploadFile:filePath videoName:videoName completion:^(BOOL success) {
        if (success) {
            [[NSFileManager defaultManager] removeItemAtURL:filePath error:nil];
            [weakSelf postVideoToPitcheroWithSuccess:successBlock failure:failureBlock];
        }
        else
        {
            failureBlock();
        }
    }];
}

- (void)setupVideoToUploadModelWithTitle:(NSString *)title
                             description:(NSString *)description
                           videoFilePath:(NSURL *)videoFilePath
                               videoName:(NSString *)videoName
                                 success:(void (^)())successBlock
                                 failure:(void (^)())failureBlock
{
    self.videoToUploadModel = [[VideoModel alloc] init];
    self.videoToUploadModel.videoTitle = title;
    self.videoToUploadModel.videoDescription = description;
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:videoFilePath options:nil];
    NSArray *tracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    AVAssetTrack *track = [tracks objectAtIndex:0];
    CGSize mediaSize = track.naturalSize;
    
    self.videoToUploadModel.height = mediaSize.height;
    self.videoToUploadModel.width = mediaSize.width;
    self.videoToUploadModel.duration = track.timeRange.duration.value;
    self.videoToUploadModel.origin = @"play_app";
    
    SourceModel *source = [[SourceModel alloc] init];
    source.videoKey = videoName;
    source.thumbnailKey = videoName; // ??
    
    self.videoToUploadModel.source = source;
}

- (void)postVideoToPitcheroWithSuccess:(void (^)())successBlock
                               failure:(void (^)())failureBlock
{
    __weak typeof (self) weakSelf = self;
    
    [self.pitcheroDataProvider postVideoToPitchero:self.videoToUploadModel successBlock:^(NSString *videoId) {
        weakSelf.videoToUploadModel.videoId = videoId;
        [weakSelf createMatchEventWithSuccess:successBlock failure:failureBlock];
        
    } failureBlock:^{
        failureBlock();
    }];
}

- (void)createMatchEventWithSuccess:(void (^)())successBlock
                            failure:(void (^)())failureBlock
{
    __weak typeof (self) weakSelf = self;
    
    [self.pitcheroDataProvider createMatchPlayerEventsForTeamId:kTeamId fixtureId:kFixtureId seconds:self.seconds successBlock:^(NSString *playerEventId) {
        weakSelf.videoToUploadModel.playerEventId = playerEventId;
        [weakSelf attachVideoToMatchEventWithSuccess:successBlock failure:failureBlock];
        
    } failureBlock:^{
        failureBlock();
    }];
}

- (void)attachVideoToMatchEventWithSuccess:(void (^)())successBlock
                                   failure:(void (^)())failureBlock
{
    [self.pitcheroDataProvider attachVideoToMatchEventsWithPlayerEventId:self.videoToUploadModel.playerEventId videoEventId:self.videoToUploadModel.videoId successBlock:^{
        successBlock();
    } failureBlock:^{
        failureBlock();
    }];
}

@end
